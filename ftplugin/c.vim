" Only do this when not done yet for this buffer
if exists("b:did_ftplugin_local")
  finish
endif

" Don't load another plugin for this buffer
let b:did_ftplugin_local = 1

abbreviate #i #include
abbreviate #d #define

setlocal shiftwidth=4
setlocal tabstop=4
setlocal cinoptions+=(2s:0cs
setlocal cindent
setlocal number

" Doxygen minimal syntax
syn region doxygenComment start=+/\*\*+ end=+\*/+ contains=doxygenTag
syn match doxygenTag display contained "[@\\][{}[:alpha:]]\+"
hi link doxygenComment Comment
hi doxygenTag cterm=bold ctermfg=1

" Useless spaces
hi useless ctermbg=1 ctermfg=0
"syn match uSpaces display "^\t* \+\|[ \t]\+$"
syn match uSpaces display "[ \t]\+$"
hi link uSpaces useless

" Glibc-devel tagsfile
set tags+=$HOME/projects/glibc.tags

syn keyword cConstant SIGIO
syn keyword cRepeat foreach
syn match cppStl display "std::[[:alpha:]_]\+"
hi link cppStl cppType

" Catch doxygen tweaked errors
" set errorformat+=%*:%f:%l:%m
" Catch gsoap errors
set errorformat+=%f(%l):%m

" QtTestLib error messages
set errorformat^=%E%>FAIL!\ \ :\ %m,%Z\ \ \ Loc:\ [%f(%l)],%C\ \ \ %m,%C\ \ \ %m

if filereadable(glob('*.pro'))
	" probably a Qt project, set specific options
	set expandtab
endif
