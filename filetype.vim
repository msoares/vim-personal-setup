"personal filetype file
"

augroup filetypedetect
  au BufNewFile,BufRead CMakeLists.txt  setfiletype cmake

  au BufNewFile,BufRead docker-compose.yml  setfiletype docker_compose
  au BufNewFile,BufRead docker-compose.yml  set syntax=yaml

  au BufRead,BufNewFile *.md            setfiletype markdown

  au BufRead,BufNewFile psql.*          setfiletype sql
  au BufRead,BufNewFile *.qml           setfiletype javascript
  au BufRead,BufNewFile Config.in       setfiletype config

  au BufRead,BufNewFile *.t2t           setfiletype t2t

  au BufNewFile,BufRead svn-commit*.tmp setfiletype svn

  au BufNewFile,BufRead *.pr[io] setfiletype qmake
  au BufNewFile,BufRead *.qrc    setfiletype xml

  au BufRead,BufNewFile *.edc source ~/.vim/ftplugin/edc.vim

  au BufRead,BufNewFile *tmux.conf setfiletype tmux

  au BufRead,BufNewFile *.inc      setfiletype make

  au BufRead,BufNewFile Jenkinsfile		setfiletype groovy
augroup END
