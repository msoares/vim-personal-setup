if !filereadable('Makefile')
	setlocal makeprg=txt2tags\ %
endif

setlocal syntax=yaml
setlocal number
