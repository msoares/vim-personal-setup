import vim

def make_title():
    w = vim.current.window
    b = vim.current.buffer
    line, col = w.cursor
    line -= 1
    b[line] = b[line].title()

vim.command("nmap \\t :python make_title()<CR>")

class StatusLine():
    def __getitem__(self, name):
        return getattr(self, name)()

    def buffer(self):
        return "#%02n"

    def path(self):
        return "%f"

    def flags(self):
        return "%m%r%y"

    def char(self):
        return "[%bd,0x%04B]"

    def tabs(self):
        return "%{MyTabMarker()}"

    def tabmarker(self):
        count = int(vim.eval("tabpagenr('$')"))
        index = int(vim.eval("tabpagenr()"))
        if count > 1:
            return "\"[tab %d/%d]\"" % (index, count)
        return "\"\""

    def location(self):
        return "%4l/%02c-%02v(%L)"

    def position(self):
        return "%P"

    def __str__(self):
        return "%(buffer)s\ %(path)s\ %(flags)s\ %(tabs)s%%=%(char)s\ %(location)s\ %(position)s" % self

vim.command("""
function! MyTabMarker()
py3 vim.command("return " + StatusLine().tabmarker())
endfunction
""")
vim.command("set statusline=" + str(StatusLine()))
#vim.command("echo \"foobar: " + str(StatusLine().tabmarker()) + "\"")
#vim.command("echo \"" + str(StatusLine()) + "\"")
