if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim  "Reset compatible-options to its Vim default value.

" prepend user runtime
set runtimepath^=~/.vim

" abbreviations
"
"iabbrev /me Milton Soares Filho

" mappings
"
" Open current file directory
nmap <F3> :e %:h
" quickfix mappings 
nmap <F7> :cp
nmap <F8> :cn
"nmap <F9> :r ~/.signature
"scrolls in terminal-mode
tmap <S-PageUp> <C-w>N

" options
"
set background=dark
set backspace=indent,eol,start
set directory=~/tmp,/tmp
set encoding=utf-8 fileencodings=utf-8,latin1 termencoding=utf8 "forces using utf-8
set formatoptions=tcql
set history=50
set hlsearch
set ignorecase
set lazyredraw
set laststatus=2 " statusline/tabline setup
set modeline
set nobackup writebackup
set nostartofline
set number
set pastetoggle=<F12>
set report=0
set ruler
set shiftwidth=4
set showcmd
set showtabline=0
set smartcase
set smartindent
set spelllang=en
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl
set suffixes+=.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set tabstop=4
set textwidth=80
set timeoutlen=500 "timeout for completing mapping sequences
set updatetime=2000
set viminfo='20,\"50
set whichwrap=<,>,[,],h,l

" complete options
set completeopt=menuone,preview
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

"terminal mode
autocmd TerminalOpen * setlocal nonumber

" generic syntax stuff
"colorscheme peachpuff
syntax on

au WinEnter,BufRead * setlocal cursorline
au WinLeave * setlocal nocursorline
hi CursorLine cterm=bold term=bold
hi CursorLineNr ctermfg=3 cterm=bold ctermbg=7
"hi Search ctermbg=NONE cterm=reverse
hi clear SignColumn

" http://vim.wikia.com/wiki/Cscope
if has('cscope')
  set cscopetag
  "set cscopeverbose

  if has('quickfix')
    set cscopequickfix=s-,c-,d-,i-,t-,e-
  endif

  if filereadable('cscope.out')
      csc add cscope.out
  endif

  " these break search
  "cnoreabbrev csa cs add
  "cnoreabbrev csf cs find
  "cnoreabbrev csk cs kill
  "cnoreabbrev csr cs reset
  "cnoreabbrev css cs show
  "cnoreabbrev csh cs help
endif

" Don't need a permament storage for this
let g:netrw_home = '/tmp'

let bundles = expand("~/.vim/bundles.vim")
if filereadable(bundles)
    silent! execute 'source '.bundles
endif

filetype plugin on
filetype plugin indent on
