" https://github.com/junegunn/vim-plug
" curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"
" On first install:
"
" :call PlugInstall

call plug#begin('~/.vim/bundles')

Plug 'junegunn/vim-plug'

let g:gitgutter_sign_removed = '- '
let g:gitgutter_sign_modified_removed = '~-'
let g:gitgutter_eager = 0
" A Vim plugin which shows a git diff in the gutter (sign column) and
" stages/reverts hunks
Plug 'airblade/vim-gitgutter', { 'as': 'gitgutter' }

Plug 'https://msoares@bitbucket.org/msoares/vimplate-personal-bundle.git', { 'as': 'vimplate' }

Plug 'msanders/snipmate.vim', { 'as': 'snipmate' }

Plug 'tpope/vim-fugitive'

Plug 'vim-scripts/FSwitch'

Plug 'jparise/vim-graphql', { 'as': 'graphql', 'for': 'graphql' }

let g:ycm_extra_conf_globlist = ['~/.ycm_extra_conf.py']
let g:ycm_filepath_completion_use_working_dir = 1
let g:ycm_auto_trigger = 0
hi! link YcmErrorSection cError
" A code-completion engine for Vim
Plug 'Valloric/YouCompleteMe', {
    \ 'do': 'python3 ./install.py --clangd-completer',
    \ 'for': [ 'c', 'cpp', 'python', ]
    \ }

let g:skyline_fugitive = 1
let g:skyline_linecount = 1
Plug 'ourigen/skyline.vim', {
	\ 'as':'skyline',
	\ 'do':'patch -p1 < ../../bundles/skyline.patch',
	\ }

Plug 'mtdl9/vim-log-highlighting'

Plug 'aklt/plantuml-syntax'

call plug#end()
