" Vim syntax file
" Filename:	txt666.vim
" Language:	plain text
" Maintainer:	aur�lio marinho jargas aurelio@conectiva.com.br
" Last change:	qui jan 11 00:47:52 EST 2001
"coloque em seu ~/.vimrc:
" au BufNewFile,BufRead *.txt  so ~/rota/para/o/txt666.vim
"
"well, some tag names are in portuguese, but in general
"  s/Branco/whitespace/
"  s/Linha/line/
"  s/Porcento/percent/
"  s/Tit/title/
"  s/Negrito/bold/
"  s/Aspas/quotes/
"  s/Palavra/word/
"should help...

" remove any old syntax stuff hanging around
syn clear

" general
syn match   txt666Item     "^\s*[-+=] [^ ]"me=e-1
syn match   txt666Header   "^\* .*" contains=txt666Email
syn match   txt666Branco   "\s\+$"
syn match   txt666Linha    "^\s*[-+=_]\+\s*$"
syn match   txt666Linha    "|"
syn match   txt666Num      "\d\+\([,.]\d\+\)\{,1}"
syn match   txt666Porcento "\d\+\([,.]\d\+\)\{,1}%"

" txt666 syntax is case sensitive
" [:upper:] and [:alnum:] are used for the accents
syn case match
syn match   txt666Tit      '^[[:alnum:][:blank:]_]\{1,20}:\([[:blank:]]\|$\)'
syn match   txt666Tit      '^\s*[[:upper:]0-9]\+\([[:blank:]-]\+[[:upper:]0-9]\+\)*$'
syn match   txt666Tit      '^\s*-- [[:upper:]0-9 ]\+ --$'
syn match   txt666Tag      "^\s*%%imagem\s*:"

syn match   txt666Email    '<\=\<[A-Za-z0-9_.-]\+@\([A-Za-z0-9_-]\+\.\)\+[A-Za-z]\+\>>\='
syn match   txt666Url      '\<\(\(https\{0,1}\|ftp\|news\)://\|\(www[23]\{0,1}\.\|ftp\.\)\)[A-Za-z0-9._/~:,-]\+\>'

syn match   txt666Negrito  '\*[^*]\+\*'
syn match   txt666Negrito2  '\*\*[^*]\+\*\*'
syn match   txt666Sublin   '\W_[^_]\+_\W'ms=s+1,me=e-1
syn match   txt666Italico  '\W//[^/]\+//\W'ms=s+1,me=e-1
syn match   txt666Ref      '\W\^[^^]\+\^\W'ms=s+1,me=e-1

"syn match   txt666Aspas1   "'[^']*'"
syn match   txt666Aspas1   "`[^`]*`"
syn region  txt666Aspas2   start=+"+  skip=+\\"+  end=+"+
syn region  txt666PreForm  start='^=--'  end='^=--'

syn match   txt666Prompt '^ \{4\}[^ =].*'
syn match   txt666Prompt '^ \{4\}prompt\$.*'
syn match   txt666Comment  '^\s*//.*$'

syn match	txt666Replyed '^>[[:print:]]\+$'

" txt666 syntax is NOT case sensitive
syn case ignore
syn keyword txt666Palavra  milton soares filho mil
syn keyword txt666Palavra  conectiva cl

" color groups
hi      txt666_Destaque ctermfg=white
hi      txt666_Link     ctermfg=lightblue
hi		txt666Replyed	ctermfg=lightmagenta

" color definitions
hi      txt666Item     ctermfg=yellow
hi      txt666Header   ctermfg=lightred
hi      txt666Branco   ctermfg=white       ctermbg=red
hi link txt666Linha    txt666_Destaque
hi      txt666Num      ctermfg=lightgreen
hi      txt666Porcento ctermfg=darkgreen

hi      txt666Tit      ctermfg=darkcyan

hi link txt666Negrito  txt666_Destaque
hi link txt666Negrito2 txt666_Destaque
hi link txt666Italico  txt666_Destaque
hi link txt666Sublin   txt666_Destaque
hi link txt666Palavra  txt666_Destaque
hi link txt666Prompt   txt666_Destaque

hi      txt666Aspas1   ctermfg=lightcyan
hi      txt666Aspas2   ctermfg=lightcyan
hi link txt666Email    txt666_Link
hi link txt666Url      txt666_Link
hi link txt666Ref      txt666_Link

hi      txt666Tag      ctermfg=yellow
hi      txt666Comment  ctermfg=brown
hi      txt666PreForm  ctermfg=green

let b:current_syntax = 'text'
